import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MainApp 
{	
	public static void main(String args[]) throws IOException
	{
		MainApp theApp = new MainApp();
		
		theApp.run(args);
	}
	
	private void run(String args[])
	{	
		// Set the proxy for DkIT
//		setProxy();
		
		// Regex to match <a href>s ending with a .XML
		String regexXML = "(<a href=\")([a-zA-Z0-9%-_ /]{1,})(.XML)";
		
		// Regex to get the main site
		String siteRegex = "http://www.[a-zA-Z0-9-_]{1,}.com";
		
		// Default site to download from
//		String site = "http://www.sem-o.com/marketdata/Pages/PricingAndScheduling.aspx";
		String site = args[0];
		System.out.println("Site to download from initialised as: " + site);
		
		// Scanner for user input
		Scanner kb = new Scanner(System.in);

		// bExit to keep menu looping
		boolean bExit = false;
		
		// Number of threads to use and delay before restarting the program 
        int threads = 5;
        int delayInMS = 600000;
          
        XMLDownloader xmlDl = new XMLDownloader(site, threads, regexXML, siteRegex, delayInMS);
            
        // Menu stuffs
        System.out.println("\nWelcome to the Web Downloader!! Please choose an option from below:");
        while(!bExit)
        {
        	displayMenu();
        	
            int userInput = -1;
            
            while(userInput == -1)
            {
	            try
	            {
	            	userInput = kb.nextInt();
	            }catch(InputMismatchException e)
	            {
	            	kb.next();
	            	userInput = -1;
	            	System.out.println("Not a valid input. Please input an integer value.");
	            }
            }
            
            if(userInput == 1)
            {	
            	if(xmlDl.isbRunning())
            		System.out.println("Please stop current downloader before running it again.");
            	else
            		xmlDl.run();
            }
            else if(userInput == 2)
            {
            	if(xmlDl.isbRunning())
            		System.out.println("Please stop current downloader before modifying thread values.");
            	else
            	{
	            	threads = getThreadNo();
	            	xmlDl.setThreads(threads);
            	}
            }
            else if(userInput == 3)
            {
            	System.out.println("Please note if the downloader has not fully completed " +
            			"execution when the system restart " +
            			"occurs, it will be stopped and a fresh execution will begin.");
	            delayInMS = getUpdateInterval();
	            xmlDl.setDelay(delayInMS);
            }
            else if(userInput == 4)
            {
                System.out.println("Stopping the Downloader");
	            xmlDl.stop();
            }
            else if(userInput == 5)
            {
                System.out.println("Goodbye!!!");
                
                xmlDl.stop();
                
                bExit = true;
            }
            else
            {
                System.out.println("Wrong input, please try again.");
            } 
        }
        kb.close();
    }
	
	
	/**
	 * Sets the proxy for DkITs network
	 */
	private void setProxy() 
	{
		String host = "proxy.dkit.ie";
		String port = "3128";
		HTTPUtility.SetProxy(host, port);
	}

	/**
	 * Prints the menu choices to the user
	 */
	private void displayMenu() 
	{
        System.out.println("1) Run downloader (Default site)");
        System.out.println("2) Change number of download threads");
        System.out.println("3) Change update period");
        System.out.println("4) End current threads");
        System.out.println("5) Exit program");
	}
	
	/**
	 * Prompts the user to enter the number of threads to use in the program
	 * @return The user defined number of threads or 0 if less than 0
	 */
	private int getThreadNo()
	{
		Scanner kb = new Scanner(System.in);
		System.out.println("Yo enter the number of threads to use (0 for whatever)");
		
		int num = -1;
        while(num == -1)
        {
            try
            {
            	num = kb.nextInt();
            }catch(InputMismatchException e)
            {
            	kb.next();
            	num = -1;
            	System.out.println("Not a valid input. Please input an integer value.");
            }
        }

		return (num<0) ? 0 : num;
	}
	
	/**
	 * Prompts the user to enter the number of threads to use in the program
	 * @return The user defined update interval or 5 minutes if less than 0
	 */
	private int getUpdateInterval()
	{
		Scanner kb = new Scanner(System.in);
		System.out.println("Enter update interval");
		int interval = -1;
        while(interval == -1)
        {
            try
            {
            	interval = kb.nextInt();
            }catch(InputMismatchException e)
            {
            	kb.next();
            	interval = -1;
            	System.out.println("Not a valid input. Please input an integer value.");
            }
        }
		
		return (interval<0) ? 300000 : interval;
	}
}
