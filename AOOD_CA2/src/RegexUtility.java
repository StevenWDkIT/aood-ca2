import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtility 
{
	/**
	 * Replaces all instances of the regular expression in the input data with the replacement data.
	 * @param strRegex	String regular expression to be applied to the data.
	 * @param strData	String data.
	 * @param strReplace	String replacement for the matched string data
	 * @return	String with the matched text replaced.
	 */
	public static String replaceAll(String strRegex, String strData, String strReplace)
	{
		return strData.replaceAll(strRegex, strReplace);
	}
	
	
	/**
	 * Tests if data matches a regular expression. 
	 * @param strRegex	String regular expression to be applied to the data.
	 * @param strData	String data.
	 * @return	True if match, otherwise false
	 */
	public static boolean matches(String strRegex, String strData)
	{
		if((strData == null) || (strData.length() == 0))
			return false;
		
		if((strRegex == null) || (strRegex.length() == 0))
			return false;
		
		return strData.matches(strRegex);
	}
	

	/**
	 * Performs the find after a user-defined start position (i.e. allows us to "jump over" string content).
	 * @param strRegex 	String regular expression to be applied to the data.
	 * @param strData 	String data.
	 * @param groupNumber	Integer specifying the group number - if the regex contains group (i.e. bracket pairs) we can specify which component to output.	
	 * @param startPosition	Integer specifying the position in the input data to start applying the regex. 
	 */
	public static void find(String strRegex, String strData, int groupNumber, int startPosition) 
	{
		Pattern pattern = Pattern.compile(strRegex);
		Matcher match = pattern.matcher(strData);
		
		while(match.find(startPosition))
		{
			System.out.println(match.group(groupNumber));
		}	
	}
	
	public static String getString(String strRegex, String strData)
	{
		StringBuilder strBld = new StringBuilder();
		Pattern pattern = Pattern.compile(strRegex);
		Matcher match = pattern.matcher(strData);
		
		if(match.find())
		{
			strBld.append(match.group());
			return strBld.toString();
		}
		else 
			return null;
	}
	
	/**
	 * Performs the find and output the specified group.
	 * @param strRegex 	String regular expression to be applied to the data.
	 * @param strData 	String data.
	 * @param groupNumber	Integer specifying the group number - if the regex contains group (i.e. bracket pairs) we can specify which component to output.	
	 */
	public static void find(String strRegex, String strData, int groupNumber)
	{
		Pattern pattern = Pattern.compile(strRegex);
		Matcher match = pattern.matcher(strData);
		
		while(match.find())
		{
			System.out.println(match.group(groupNumber));
		}	
	}
	
	/**
	 * Performs the find on the input data with a regex
	 * @param strRegex 	String regular expression to be applied to the data.
	 * @param strData 	String data.
	 */
	public static void find(String strRegex, String strData)
	{
		Pattern pattern = Pattern.compile(strRegex);
		Matcher match = pattern.matcher(strData);
		
		while(match.find())
		{
			System.out.println(match.group());
		}	
	}
	
	/**
	 * Returns the data found in a custom wrapper class RegexOutput.
	 * @param strRegex 	String regular expression to be applied to the data.
	 * @return	An ArrayList of RegexOutput objects, zero length if no match
	 */
	public static ArrayList<RegexOutput> findWithPosition(String strRegex, String strData) {
		if((strData == null) || (strRegex == null))
			return null;
		
		if((strData.length() == 0) || (strRegex.length() == 0))
			return null;
		
		ArrayList<RegexOutput> list 
						= new ArrayList<RegexOutput>();
		Pattern pattern = Pattern.compile(strRegex);
		Matcher match = pattern.matcher(strData);
		
		while(match.find())
		{
			list.add(new RegexOutput(
					match.group(),
					match.start(),
					match.end())
					);
		}	
		return list;
	}
	
}
