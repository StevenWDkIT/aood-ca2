import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

public class ThreadManager
{
	private int threadNo;
	private XMLTask threads[];
	private ArrayList<String> XMLLinks;
	private String directory;

	
	/**
	 * Constructor for the ThreadManager
	 * @param threads - The number of threads to execute at once
	 * @param dir - The path to the directory to save to
	 */
	public ThreadManager(int threads, String dir)
	{
		this.XMLLinks = new ArrayList<String>();
		this.threadNo = threads;	
		this.directory = dir;
	}
	
	/**
	 * Setter to set the ArrayList of links. Also updates threads[]'s size relative to the size of the new ArrayList.  
	 * @param links - An ArrayList containing formatted links to download and save
	 */
	public void setLinks(ArrayList<String> links)
	{
		this.XMLLinks = links;
		
		if(this.threadNo == 0)
			this.threads = new XMLTask[this.XMLLinks.size()];
		else if(this.threadNo > this.XMLLinks.size())
		{
			setThreads(this.XMLLinks.size());
			this.threads = new XMLTask[this.threadNo];
		}
		else
			this.threads = new XMLTask[this.threadNo];
	}
	
	/**
	 * Setter for the number of threads to use. Updates the size of threads[] after setting threadNo.
	 * @param threads - The number of threads to use
	 */
	public void setThreads(int threads)
	{
		this.threadNo = threads;
		
		if(this.threadNo == 0)
		{
			this.threadNo = this.XMLLinks.size();
			this.threads = new XMLTask[this.threadNo];
		}
		else if(this.threadNo > this.XMLLinks.size())
		{
			this.threadNo = this.XMLLinks.size();
			this.threads = new XMLTask[this.threadNo];
		}
		else
			this.threads = new XMLTask[this.threadNo];
	}
	
	/**
	 * The main method of ThreadManager. Creates a directory at C:/temp/{current system time} to save the files to.
	 * Then creates the specified number of XMLTasks and starts them.
	 */
	public void start()
	{
		Calendar c = Calendar.getInstance();
		setDirectory(c.get(Calendar.DATE) + "-" + (c.get(Calendar.MONTH)+1) + "-" + c.get(Calendar.YEAR) + " " + 
				 c.get(Calendar.HOUR_OF_DAY) + "," + c.get(Calendar.MINUTE) + "," + c.get(Calendar.SECOND));
		File dir = new File(this.directory);

		dir.mkdir();
		
		// Start the threads

		for(int i = 0; i < this.threadNo; i++)
		{
			this.threads[i] = new XMLTask(this.XMLLinks, this.directory);
			this.threads[i].start();
		}
			
			
		System.out.println("\n" + this.threadNo + " threads started downloading and saving.");
	}

	/**
	 * Setter for the directory to save to
	 * @param directory - The path to the directory to save to post C:/temp
	 */
	private void setDirectory(String directory) 
	{
		this.directory = "C:\\temp\\" + directory;
	}

	/**
	 * Stops all of the XMLTasks and prints out the number of files currently saved and the location where they are saved.
	 * Loops through threads[] and calls the {@link XMLTask#setStop() setStop()} method for each one to gracefully exit the threads.
	 */
	public void stop() 
	{
		for(XMLTask t : this.threads)
			t.setStop();
		
		System.out.println(new File(this.directory).list().length + " XML Files saved by this execution.");
		System.out.println("The system will complete the downloads currently in progress (if any) before halting.");
		System.out.println("Files saved to \'" + this.directory + "\'");
	}
	
}
