import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Steven
 *
 */
public class XMLDownloader 
{
	private ThreadManager threadManager;
	private ArrayList<RegexOutput> XMLLinks;
	private String site;
	private String XMLRegex;
	private String siteRegex;
	private Timer timer;
	private int delayInMS;
	private String directory;
	private boolean bRunning;
	
	
	/**
	 * Constructor for the XMLDownloader.
	 * 
	 * @param site - The site to download the XML files from
	 * @param numberOfThreads - The amount of threads to do the downloading
	 * @param XMLRegex - The Regex to collect the XML links from the page source
	 * @param siteRegex - The Regex to collect the site's domain
	 * @param delayInMS - The time before the program restarts itself
	 */
	public XMLDownloader(String site, int numberOfThreads, String XMLRegex, String siteRegex, int delayInMS)
	{
		this.site = site;
		this.XMLRegex = XMLRegex;
		this.siteRegex = siteRegex;
		this.XMLLinks = new ArrayList<RegexOutput>();
		this.delayInMS = delayInMS;
		this.bRunning = false;
		this.timer = new Timer();
		this.directory = "C:\\temp\\";
		this.threadManager = new ThreadManager(numberOfThreads, this.directory);
		initialise();
	}
	
	/**
	 * Initialises this XMLDownloader by storing the links found on the page via 
	 * {@link RegexUtility#findWithPosition findWithPosition()}. 
	 * Also creates a directory at C:/temp to save files to and calls the 
	 * {@link XMLDownloader#initialiseThreadManager initialiseThreadManager()} method.
	 */
	private void initialise() 
	{
		this.XMLLinks = RegexUtility.findWithPosition(this.XMLRegex, HTTPUtility.download(site).getHTMLData().toString());
		
		File temp = new File(this.directory);
		temp.mkdir();
		
		initialiseThreadManager();
	}

	/**
	 * Formats the links to a format that would be accepted by {@link HTTPUtility#download() download()}
	 * by replacing any spaces in the URL with a %20. Then the domain gotten with the SiteRegex is added to
	 * the links stored earlier to create the full link. Calls the {@link ThreadManager#setLinks(ArrayList) ThreadManager.setLinks()}
	 * method to set the ArrayList<> of links to the new formatted links.
	 */
	private void initialiseThreadManager()
	{
		String tempStr;
		String domain = RegexUtility.findWithPosition(this.siteRegex, this.site).get(0).getData();
		ArrayList<String> stringLinks = new ArrayList<String>();

		for(RegexOutput r : this.XMLLinks)
		{
			tempStr = r.getData().substring(10);
			tempStr = tempStr.replaceAll(" ", "%20");
			stringLinks.add(domain + "/" + tempStr);
		}
		
		this.threadManager.setLinks(stringLinks);
	}
	
	/**
	 * Restarts the XMLDownloader by stopping the current execution via {@link XMLDownloader#stop() stop()} 
	 * and calling the {@link XMLDownloader#run() run()} method.
	 */
	private void restart()
	{
		stop();
		run();
	}
	
	/**
	 * @return Returns if the XMLDownloader is currently running
	 */
	public boolean isbRunning() 
	{
		return this.bRunning;
	}
	
	/**
	 * If the XMLDownloader isn't already running, calls {@link XMLDownloader#initialise() initialise()} and schedules the XMLDownloader to
	 * restart at the time specified by {@link XMLDownloader#delayInMS delayInMS}. The bRunning is set to true.
	 */
	public void run()
	{
		if(!this.bRunning)
		{
			initialise();
			
			this.threadManager.start();
		}
		
		timer = new Timer();
		timer.schedule(new TimerTask(){public void run(){restart(); }}, this.delayInMS);

		this.bRunning = true;
	}

	/**
	 * Cancels the current Timer set to restart the XMLDownloader. If the XMLDownloader is currently executing it stops the ThreadManager.
	 * The bRunning is updated to false.
	 */
	public void stop() 
	{
		timer.cancel();
		
		if(this.bRunning)
			this.threadManager.stop();
		
		this.bRunning = false;
	}

	/**
	 * Setter for the number of threads to use.
	 * @param threads - The new number of threads to use
	 */
	public void setThreads(int threads) 
	{
//		initialise();
		this.threadManager.setThreads(threads);
	}

	/**
	 * Setter for the restart delay.
	 * @param delayInMS - The new restart period in MS
	 */
	public void setDelay(int delayInMS) 
	{
		this.delayInMS = delayInMS;
	}
}
