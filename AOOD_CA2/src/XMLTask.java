import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class XMLTask extends Thread
{
	private ArrayList<String> links;
	private String savePath;
	
	private volatile boolean bStop = false;
	
	/**
	 * Constructor for the XMLTask
	 * @param links - An ArrayList of formatted links to XML Files
	 * @param savePath - A path to a directory to write to
	 */
	public XMLTask(ArrayList<String> links, String savePath)
	{
		this.links = links;
		this.savePath = savePath;
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() 
	{
		while(!getStop() && this.links.size() > 0)
		{
			String link = getAndRemoveLink();
			if(link != null)
			{
				String fileName = link.substring(48, link.indexOf('.', 48)).replaceAll("%20", " ");
				
				HTTPData XMLFile = HTTPUtility.download(link);
				File f = new File(this.savePath + "/" + fileName + ".XML");
				
			    try 
			    {
			    	FileWriter fw = new java.io.FileWriter(f);
			        fw.write(XMLFile.getHTMLData().toString());
			        fw.close();
			        
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * A synchronized method to pull a link from an ArrayList<String>.
	 * @return The string removed from the ArrayList
	 */
	private synchronized String getAndRemoveLink() 
	{
		if(this.links.size() > 0)
		{
			return this.links.remove(0);
		}
		else
			return null;
	}

	/**
	 * Setter for bStop. Sets the value to true.
	 */
	public void setStop()
	{
		this.bStop = true;
	}
	
	/**
	 * Getter for bStop.
	 * @return bStop, if the thread is destined to stop.
	 */
	public boolean getStop()
	{
		return this.bStop;
	}
}












